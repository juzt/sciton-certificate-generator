<? 
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Localization\Loc;

$module_id = 'certificates';

$moduleAccess = $APPLICATION->GetGroupRight($module_id);

$options = array(
	"number"
);

if($REQUEST_METHOD=="POST")
{
    for ($i=0; $i < count($options); $i++)
    {
        $val = htmlspecialchars($_POST[$options[$i]]);
        COption::SetOptionString($module_id, $options[$i], $val);
    }
}

if ($moduleAccess >= "W") {
	Loader::includeModule($module_id);

	$aTabs = array(
		array("DIV" => "edit1", "TAB" => "Настройки", "ICON" => "", "TITLE" => "Настройки"),
		array("DIV" => "edit2", "TAB" => "Права доступа", "ICON" => "", "TITLE" => "Права доступа")
	);

	$tabControl = new CAdminTabControl("tabControl", $aTabs);

	$tabControl->Begin();
	?>
	<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($module_id)?>&amp;lang=<?=LANGUAGE_ID?>">
	<?$tabControl->BeginNextTab();?>

		<tr>
			<td>Нумерация:</td>
			<td><input type="text" name="number" id="number" value="<?=COption::GetOptionString($module_id, 'number')?>"></td>
		</tr>

	<?$tabControl->BeginNextTab();?>
		<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");?>

		<?$tabControl->Buttons();?>
		<input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
		<?=bitrix_sessid_post();?>
		<?if($_REQUEST["back_url_settings"] <> ''):?>
			<input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" onclick="window.location='<?echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
			<input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
		<?endif;?>
	<?$tabControl->End();?>
	</form>
<? } ?>