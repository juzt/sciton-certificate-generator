INSERT INTO b_iblock_type (ID, SECTIONS, IN_RSS) VALUES('certificates_generator', 'Y', 'N');
INSERT INTO b_iblock_type_lang (IBLOCK_TYPE_ID, LID, NAME) VALUES('certificates_generator', 'ru', 'Генератор сертификатов');
INSERT INTO b_iblock_type_lang (IBLOCK_TYPE_ID, LID, NAME) VALUES('certificates_generator', 'en', 'Certificates generator');

INSERT INTO b_iblock (IBLOCK_TYPE_ID, LID, CODE, NAME, ACTIVE) VALUES('certificates_generator', 's1', 'certificates_generator', 'Шаблоны сертификатов', 'Y');
INSERT INTO b_iblock (IBLOCK_TYPE_ID, LID, CODE, NAME, ACTIVE) VALUES('certificates_generator', 's1', 'certificates_generator_log', 'История генераций', 'Y');
INSERT INTO b_iblock (IBLOCK_TYPE_ID, LID, CODE, NAME, ACTIVE) VALUES('certificates_generator', 's1', 'certificates_generator_types', 'Виды сертификатов', 'Y');
INSERT INTO b_iblock (IBLOCK_TYPE_ID, LID, CODE, NAME, ACTIVE) VALUES('certificates_generator', 's1', 'certificates_generator_doctors', 'Врачи', 'Y');

INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Подпись', ACTIVE='Y', CODE='SIGNATURE', PROPERTY_TYPE='F', IS_REQUIRED='N', FILE_TYPE='png';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Подпись (координаты)', ACTIVE='Y', CODE='SIGNATURE_COORDS', PROPERTY_TYPE='S', IS_REQUIRED='N';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='ФИО участника (координаты)', ACTIVE='Y', CODE='FIO_PERSON_COORDS', PROPERTY_TYPE='S', IS_REQUIRED='N';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='ФИО (координаты)', ACTIVE='Y', CODE='FIO_COORDS', PROPERTY_TYPE='S', IS_REQUIRED='N';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Вид сертификата (координаты)', ACTIVE='Y', CODE='TYPE_COORDS', PROPERTY_TYPE='S', IS_REQUIRED='Y';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Номер сертификата (координаты)', ACTIVE='Y', CODE='CERTIFICATE_ID_COORDS', PROPERTY_TYPE='S', IS_REQUIRED='Y';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Дата', ACTIVE='Y', CODE='DATE', PROPERTY_TYPE='S', IS_REQUIRED='N', USER_TYPE='Date', HINT='если не введена, то будет подставлена текущая дата';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Дата (координаты)', ACTIVE='Y', CODE='DATE_COORDS', PROPERTY_TYPE='S', IS_REQUIRED='N';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Размер шрифта', ACTIVE='Y', CODE='FONT_SIZE', PROPERTY_TYPE='S', IS_REQUIRED='Y';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='Сертификат', ACTIVE='Y', CODE='PDF', PROPERTY_TYPE='F', FILE_TYPE='jpeg,jpg,png', IS_REQUIRED='Y';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator'), NAME='JSON позиционирование', ACTIVE='Y', CODE='POSITIONS', PROPERTY_TYPE='F', FILE_TYPE='json', IS_REQUIRED='Y';

INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator_log'), NAME='PDF', ACTIVE='Y', CODE='PDF', PROPERTY_TYPE='F', FILE_TYPE='pdf', IS_REQUIRED='N';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator_log'), NAME='PDF без фона', ACTIVE='Y', CODE='PDF_NO_BG', PROPERTY_TYPE='F', FILE_TYPE='pdf', IS_REQUIRED='N';
INSERT INTO b_iblock_property SET IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator_log'), NAME='Получатель', ACTIVE='Y', CODE='FIO', PROPERTY_TYPE='S', IS_REQUIRED='N';