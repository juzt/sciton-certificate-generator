DELETE FROM b_iblock_type_lang WHERE IBLOCK_TYPE_ID='certificates_generator';
DELETE FROM b_iblock_type WHERE ID='certificates_generator';

DELETE FROM b_iblock_element WHERE IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator');
DELETE FROM b_iblock_property WHERE IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator');
DELETE FROM b_iblock WHERE CODE='certificates_generator';

DELETE FROM b_iblock_element WHERE IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator_log');
DELETE FROM b_iblock_property WHERE IBLOCK_ID=(SELECT ID FROM b_iblock WHERE CODE='certificates_generator_log');
DELETE FROM b_iblock WHERE CODE='certificates_generator_log';

DELETE FROM b_iblock WHERE CODE='certificates_generator_types';
DELETE FROM b_iblock WHERE CODE='certificates_generator_doctors';