<?
	Class certificates extends CModule
	{
	var $MODULE_ID = "certificates";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;

	function certificates()
	{
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = "Certificates – модуль создания сертификатов";
		$this->MODULE_DESCRIPTION = "После установки вы сможете пользоваться страницей /certificates_generator/";
	}

	function InstallFiles()
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/certificates/install/pages", $_SERVER["DOCUMENT_ROOT"]."/", true, true);
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/certificates/install/components", $_SERVER["DOCUMENT_ROOT"]."/bitrix/components", true, true);
		return true;
	}

	function UnInstallFiles()
	{
		DeleteDirFilesEx("/certificates_generator");
		DeleteDirFilesEx("/bitrix/components/juzt/certificates");
		return true;
	}

	function InstallDB()
	{
		global $DB, $DBType, $DBHost, $DBLogin, $DBPassword, $DBName, $APPLICATION;

		if (!is_object($APPLICATION))
			$APPLICATION = new CMain;

		$DB = new CDatabase;
		$DB->DebugToFile = false;
		$DB->debug = true;

		if (!defined("DBPersistent"))
			define("DBPersistent", false);

		if (!$DB->Connect($DBHost, $DBName, $DBLogin, $DBPassword))
		{
			$APPLICATION->ThrowException(GetMessage("MAIN_INSTALL_DB_ERROR"));
			return false;
		}

		if ($DBType == "mysql" && defined("MYSQL_TABLE_TYPE") && MYSQL_TABLE_TYPE <> '')
			$DB->Query("SET storage_engine = '".MYSQL_TABLE_TYPE."'", true);

		$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/certificates/install/".$DBType."/install.sql");

		if ($errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $errors));
			return false;
		}

		return true;
	}

	function UnInstallDB()
	{
		global $DB, $DBType, $DBHost, $DBLogin, $DBPassword, $DBName, $APPLICATION;

		if (!is_object($APPLICATION))
			$APPLICATION = new CMain;

		$DB = new CDatabase;
		$DB->DebugToFile = false;
		$DB->debug = true;

		if (!defined("DBPersistent"))
			define("DBPersistent", false);

		if (!$DB->Connect($DBHost, $DBName, $DBLogin, $DBPassword))
		{
			$APPLICATION->ThrowException(GetMessage("MAIN_INSTALL_DB_ERROR"));
			return false;
		}

		if ($DBType == "mysql" && defined("MYSQL_TABLE_TYPE") && MYSQL_TABLE_TYPE <> '')
			$DB->Query("SET storage_engine = '".MYSQL_TABLE_TYPE."'", true);

		$errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/local/modules/certificates/install/".$DBType."/uninstall.sql");

		if ($errors !== false)
		{
			$APPLICATION->ThrowException(implode("", $errors));
			return false;
		}

		return true;
	}

	function DoInstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;

		if ($result = $this->InstallDB()) {
			$this->InstallFiles();
			RegisterModule("certificates");

			$GLOBALS['CACHE_MANAGER']->cleanDir('b_iblock_type');
			$GLOBALS['CACHE_MANAGER']->cleanDir('b_iblock');

			$APPLICATION->IncludeAdminFile("Установка модуля certificates", $DOCUMENT_ROOT."/local/modules/certificates/install/step.php");
		} else {
			ShowError('Ошибка установки');
		}
	}

	function DoUninstall()
	{
		global $DOCUMENT_ROOT, $APPLICATION;

		if ($result = $this->UnInstallDB()) {
			$this->UnInstallFiles();
			UnRegisterModule("certificates");

			$GLOBALS['CACHE_MANAGER']->cleanDir('b_iblock_type');
			$GLOBALS['CACHE_MANAGER']->cleanDir('b_iblock');

			$APPLICATION->IncludeAdminFile("Деинсталляция модуля certificates", $DOCUMENT_ROOT."/local/modules/certificates/install/unstep.php");
		} else {
			ShowError('Ошибка удаления');
		}
	}
	}
?>