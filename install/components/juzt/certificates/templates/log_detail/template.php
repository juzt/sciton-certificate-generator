<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<? if (empty($arResult['ITEM'])) :?>
	<? ShowError('Сертификат не найден'); ?>
	<? return; ?>
<? endif; ?>

<? if (empty($arResult['LOG_ITEM'])) :?>
	<? ShowError('Лог сертификата не найден'); ?>
	<? return; ?>
<? endif; ?>

<a href="/certificates_generator/?id=<?=$arResult['ITEM']['ID']?>">Вернуться к генерации</a>

<div class="certificates_preview-block">
	<img src="<?=CFile::GetPath($arResult['LOG_ITEM']['PREVIEW_PICTURE'])?>" width="100%">
	<div class="certificates-buttons">
		<a href="<?=CFile::GetPath($arResult['LOG_ITEM']['PROPERTIES']['PDF']['VALUE'])?>" target="_blank" class="btn button certificates-custom-button">Просмотр</a>
		<a href="<?=CFile::GetPath($arResult['LOG_ITEM']['PROPERTIES']['PDF']['VALUE'])?>" class="btn button certificates-custom-button js-send-log" data-white="0" data-log-id="<?=$arResult['LOG_ITEM']['ID']?>" data-id="<?=$arResult['ITEM']['ID']?>" download="">Скачать с фоном</a>
		<a href="<?=CFile::GetPath($arResult['LOG_ITEM']['PROPERTIES']['PDF_NO_BG']['VALUE'])?>" class="btn button certificates-custom-button js-send-log" data-white="1" data-log-id="<?=$arResult['LOG_ITEM']['ID']?>" data-id="<?=$arResult['ITEM']['ID']?>" download="">Скачать без фона</a>
		<a href="<?=CFile::GetPath($arResult['LOG_ITEM']['PROPERTIES']['PDF']['VALUE'])?>" class="btn button certificates-custom-button js-send-log" data-white="0" data-log-id="<?=$arResult['LOG_ITEM']['ID']?>" data-id="<?=$arResult['ITEM']['ID']?>">Печать с фоном</a>
		<a href="<?=CFile::GetPath($arResult['LOG_ITEM']['PROPERTIES']['PDF_NO_BG']['VALUE'])?>" class="btn button certificates-custom-button js-send-log" data-white="1" data-log-id="<?=$arResult['LOG_ITEM']['ID']?>" data-id="<?=$arResult['ITEM']['ID']?>">Печать без фона</a>
	</div>
</div>