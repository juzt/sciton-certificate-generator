$(function() {
	$('.js-send-log').on('click', function(e) {
		e.preventDefault();

		var _this = this;

		$.ajax({
			url: window.location.pathname,
			method: 'POST',
			data: {
				id: $(this).data('id'),
				type: 'ADD_LOG',
				white: $(this).data('white'),
				text: $(this).text(),
				url: $(this).attr('href'),
				logId: $(this).data('log-id')
			},
			dataType: 'JSON',
			success: function(responce) {
				if ($(_this).is("[download]")) {
					var link = document.createElement('a');
					link.setAttribute('href', $(_this).attr('href'));
					link.setAttribute('download', $(_this).attr('href'));
					link.click();
				} else {
					printPdf($(_this).attr('href'));
				}
			}
		});
	});

	function printPdf(url) {
        var iframe = document.createElement('iframe');
        iframe.className='pdfIframe'
        document.body.appendChild(iframe);
        iframe.style.display = 'none';
        iframe.onload = function () {
            setTimeout(function () {
                iframe.focus();
                iframe.contentWindow.print();
                URL.revokeObjectURL(url);
            }, 1);
        };
        iframe.src = url;
    }
});