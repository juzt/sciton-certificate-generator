<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<? if (empty($arResult['ITEM'])) :?>
	<? ShowError('Сертификат не найден'); ?>
	<? return; ?>
<? endif; ?>

<a href="/certificates_generator/">Вернуться к списку</a>

<form class="cert_form" method="POST" action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="id" value="<?=$arResult['ITEM']['ID']?>">

	<p style="color: green" id="success-message"></p>
	<p style="color: red" id="error-message"></p>

	<img src="/bitrix/components/juzt/certificates/templates/detail/images/loader.gif" class="certificates-loader">

	<table border="0">
		<? if ($arResult['TYPES']) :?>
			<tr>
				<td><label>Вид/подвид сертификата: <span class="required_field">*</span></label></td>
				<td>
					<select class="input_text certificates-custom-input" required="required" name="certificate_type">
						<? foreach($arResult['TYPES'] as $type) :?>
							<optgroup label = "<?=$type['NAME']?>">
								<? if (!empty($type['CHILD'])) :?>
									<? foreach($type['CHILD'] as $child) :?>
										<option value="<?=$child['NAME']?>"><?=$child['NAME']?></option>
									<? endforeach; ?>
								<? endif; ?>
							</optgroup>
						<? endforeach; ?>
					</select>
				</td>
			</tr>
		<? endif; ?>
		<? if ($arResult['DOCTORS']) :?>
			<tr>
				<td><label>Врач: <span class="required_field">*</span></label></td>
				<td>
					<select class="input_text certificates-custom-input" required="required" name="certificate_doctor">
						<? foreach($arResult['DOCTORS'] as $doctor) :?>
							<option value="<?=$doctor['NAME']?>"><?=$doctor['NAME']?></option>
						<? endforeach; ?>
					</select>
				</td>
			</tr>
		<? endif; ?>
		<tr>
			<td><label>Фамилия: <span class="required_field">*</span></label></td>
			<td><input type="text" class="input_text certificates-custom-input" required="required" name="certificate_surname"></td>
		</tr>
		<tr>
			<td><label>Имя: <span class="required_field">*</span></label></td>
			<td><input type="text" class="input_text certificates-custom-input" required="required" name="certificate_name"></td>
		</tr>
		<? foreach($arResult['ITEM']['POSITIONS'] as $fieldName => $fieldValue) :?>
			<tr>
				<td><label><?=$fieldValue['name']?>: <span class="required_field">*</span></label></td>
				<td><input type="text" class="input_text certificates-custom-input" required="required" name="<?=$fieldName?>"></td>
			</tr>
		<? endforeach; ?>

		<tr>
			<td><label>Компания:</label></td>
			<td><input type="text" class="input_text certificates-custom-input" name="certificate_company"></td>
		</tr>
		<tr>
			<td><label>Клиника:</label></td>
			<td><input type="text" class="input_text certificates-custom-input" name="certificate_clinic"></td>
		</tr>
		<tr>
			<td><label>Пол:</label></td>
			<td><input type="text" class="input_text certificates-custom-input" name="certificate_gender"></td>
		</tr>
		<tr>
			<td><label>Телефон:</label></td>
			<td><input type="text" class="input_text certificates-custom-input" name="certificate_phone"></td>
		</tr>
		<tr>
			<td><label>E-mail:</label></td>
			<td><input type="email" class="input_text certificates-custom-input" name="certificate_email"></td>
		</tr>
	</table>

	<div class="certificates-buttons">
		<input class="btn button certificates-custom-button" type="submit" data-id="VIEW" value="Предпросмотр">
		<input class="btn button certificates-custom-button" type="submit" data-id="SAVE" value="Сгенерировать сертификат">
	</div>
</form>

<hr style="margin: 50px 0">

<h3>Массовая генерация</h3>

<div class="certificates_simple-download">
	<a href="<?=$APPLICATION->GetCurPage()?>" data-item="<?=$arResult['ITEM']['ID']?>" data-id="DOWNLOAD_SIMPLE">Скачать образец</a> 	&#8592; заполните образец и загрузите для генерации. <span style="color: red">Шапку в образце менять и удалять нельзя!</span>
</div>

<form class="cert_form_array" method="POST" action="<?=$APPLICATION->GetCurPage()?>">
	<input type="hidden" name="id" value="<?=$arResult['ITEM']['ID']?>">

	<p style="color: green; margin-bottom: 30px;" id="success-message-mass"></p>
	<p style="color: red; margin-bottom: 30px;" id="error-message-mass"></p>
	<img src="/bitrix/components/juzt/certificates/templates/detail/images/loader.gif" class="certificates-loader-mass">

	<input type="file" name="file" accept=".xls,.xlsx" required="">

	<input class="btn button certificates-custom-button" type="submit" data-id="MASS_GENERATOR" value="Сгенерировать сертификаты" style="margin-top: 30px; padding: 0 20px; width: auto;">
</form>

<hr class="certificates_mass-results-title" style="margin: 50px 0; display: none;">

<h3 class="certificates_mass-results-title" style="margin-bottom: 50px; display: none;">Сертификаты успешно сгенерированы:</h3>

<ul class="certificates_mass-results">
	<li>
		Сертификат Sciton - Мороз Дарья - https://sciton-russia.ru/certificates_generator/?id=277&logId=559
	</li>
	<li>
		Сертификат Sciton - Мороз Дарья - https://sciton-russia.ru/certificates_generator/?id=277&logId=559
	</li>
</ul>

<a href="/" class="btn button certificates_mass-save " style="display: none;">Скачать все</a>