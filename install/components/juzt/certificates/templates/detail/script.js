$(function() {
	$('.cert_form').on('submit', function(e) {
		e.preventDefault();

		$('#success-message').html('');
		$('#error-message').html('');
		$('.certificates-loader').show();

		$.ajax({
			url: $(this).attr('action'),
			method: 'POST',
			data: $(this).serialize() + '&type=' + $(e.originalEvent.submitter).data('id'),
			dataType: 'JSON',
			success: function(responce) {
				if (responce.success) {
					if (responce.pdf) {
						$('.certificates-loader').hide();
						window.open(responce.pdf);
						$('#success-message').html(responce.message);
						setTimeout(function() {
							$('#success-message').html('');
						}, 5000);
					} else {
						if (responce.pdfId) {
							window.location.href = window.location.href + '&logId=' + responce.pdfId;
						}

						$('.certificates-loader').hide();
						$('#success-message').html(responce.message);
						setTimeout(function() {
							$('#success-message').html('');
						}, 5000);
					}
				} else {
					$('.certificates-loader').hide();
					$('#error-message').html(responce.message);
					setTimeout(function() {
						$('#error-message').html('');
					}, 5000);
				}
			}
		});
	});

	$('[data-id="DOWNLOAD_SIMPLE"]').on('click', function(e) {
		e.preventDefault();

		$.ajax({
			url: $(this).attr('href'),
			method: 'POST',
			data: {
				type: $(this).data('id'),
				id: $(this).data('item')
			},
			dataType: 'JSON',
			success: function(responce) {
				window.open(responce.url);
			}
		});
	});

	$('.cert_form_array').on('submit', function(e) {
		e.preventDefault();

		$('.certificates_mass-results-title').hide();
		$('.certificates_mass-results').hide().find('li').remove();
		$('.certificates_mass-save').hide();

		$('#success-message-mass').html('');
		$('#error-message-mass').html('');
		$('.certificates-loader-mass').show();

		var formData = new FormData();
		formData.append('file', $(this).find('input[type=file]').prop('files')[0]);

		formData.append('type', $(e.originalEvent.submitter).data('id'));
		formData.append('id', $(this).find('input[name="id"]').val());

		console.log(formData)

		$.ajax({
			url: $(this).attr('action'),
			method: 'POST',
			data: formData,
			processData: false,
        	contentType: false,
			dataType: 'JSON',
			success: function(responce) {
				if (responce.success) {
					$('.certificates_mass-results-title').show();

					$.each(responce.ids, (function(key, value) {
						$('.certificates_mass-results').append('<li>' + value.html + '</li>');
					}));

					$('.certificates_mass-results').show();

					$('.certificates_mass-save').attr('href', responce.url).show();

					$('.certificates-loader-mass').hide();
					$('#success-message-mass').html(responce.message);
					setTimeout(function() {
						$('#success-message-mass').html('');
					}, 5000);
				} else {
					$('.certificates-loader-mass').hide();
					$('#error-message-mass').html(responce.message);
					setTimeout(function() {
						$('#error-message-mass').html('');
					}, 5000);
				}
			}
		});
	});
});