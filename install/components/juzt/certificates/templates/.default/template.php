<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>

<? if (empty($arResult['CERTIFICATES'])) :?>
	<? ShowError('Сертификаты не настроены'); ?>
	<? return; ?>
<? endif; ?>

<h3>Список шаблонов:</h3>

<ol>
	<? foreach($arResult['CERTIFICATES'] as $key => $certificate) :?>
		<li><a href="<?=$APPLICATION->GetCurPage()?>?id=<?=$certificate['ID']?>"><?=$key + 1?>. <?=$certificate["NAME"]?></a></li>
	<? endforeach; ?>
</ol>