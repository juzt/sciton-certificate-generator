<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

ini_set('memory_limit', '100M');

use Bitrix\Main\Loader;

Loader::includeModule("iblock");

class Certificates extends CBitrixComponent
{
    private $module_id = 'certificates';
    private $settings_iblock_id = 0;
    private $log_iblock_id = 0;
    private $types_iblock_id = 0;
    private $doctors_iblock_id = 0;
    private $urls = array();
    private $fio = false;

    public function executeComponent()
    {
        global $APPLICATION;

        $moduleAccess = $APPLICATION->GetGroupRight($this->module_id);

        if ($moduleAccess >= "W") {
            if($this->startResultCache())
            {
                $this->getIblockId();

                if (isset($_GET['id']) && intval($_GET['id']) > 0) {
                    $this->getItem(intval($_GET['id']));

                    if (isset($_GET['logId']) && intval($_GET['logId']) > 0) {
                        $this->getLogItem(intval($_GET['logId']));

                        $APPLICATION->SetTitle('Сгенерированный сертификат');

                        $this->setTemplateName('log_detail');
                        $this->includeComponentTemplate();
                    } else {
                        $this->getTypes();
                        $this->getDoctors();
                      
                        $APPLICATION->SetTitle($this->arResult['ITEM']['NAME']);

                        $this->setTemplateName('detail');
                        $this->includeComponentTemplate();
                    }

                    return;
                }

                if (isset($_POST['id']) && intval($_POST['id']) > 0) {
                    $APPLICATION->RestartBuffer();

                    $this->getItem(intval($_POST['id']));

                    switch ($_POST['type']) {
                        case 'VIEW':
                            $this->getView();
                            break;

                        case 'SAVE':
                            $this->getSave();
                            break;

                        case 'DOWNLOAD_SIMPLE':
                            $this->getDownloadSimple();
                            break;

                        case 'MASS_GENERATOR':
                            $this->getMassGenerator();
                            break;

                        case 'ADD_LOG':
                        	$this->getLogItem(intval($_POST['logId']));
                            $this->log(htmlspecialchars($_POST['text']), htmlspecialchars($_POST['url']), htmlspecialchars($_POST['white']), $this->arResult['LOG_ITEM']['PROPERTIES']['FIO']['VALUE']);
                            echo json_encode(array('success' => true));
                            die();
                        
                        default:
                            echo json_encode(array('success' => false));
                            break;
                    }

                    die();
                }

                $this->getSettings();

                $this->includeComponentTemplate();
            }
        } else {
            ShowError('Доступ запрещен');
        }
    }

    private function getIblockId()
    {
        $rsIblock = CIblock::GetList(array(), array('ACTIVE' => 'Y', 'CODE' => array('certificates_generator', 'certificates_generator_log', 'certificates_generator_types', 'certificates_generator_doctors')));

        while($arIblock = $rsIblock->fetch()) {
            if ($arIblock['CODE'] == 'certificates_generator') {
                $this->settings_iblock_id = $arIblock['ID'];
            }

            if ($arIblock['CODE'] == 'certificates_generator_log') {
                $this->log_iblock_id = $arIblock['ID'];
            }

            if ($arIblock['CODE'] == 'certificates_generator_types') {
                $this->types_iblock_id = $arIblock['ID'];
            }

            if ($arIblock['CODE'] == 'certificates_generator_doctors') {
                $this->doctors_iblock_id = $arIblock['ID'];
            }
        }

        if ($this->settings_iblock_id == 0 || $this->log_iblock_id == 0 || $this->types_iblock_id == 0 || $this->doctors_iblock_id == 0) {
            ShowError('Инфоблоки не найдены. Переустановите модуль');
            die();
        }
    }

    private function getSettings()
    {
        $rsSettings = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('IBLOCK_ID' => $this->settings_iblock_id, 'ACTIVE' => 'Y'), false, false, array());

        while($arSettings = $rsSettings->GetNextElement()) {
            $fields = $arSettings->getFields();
            $fields['PROPERTIES'] = $arSettings->getProperties();

            $this->arResult['CERTIFICATES'][] = $fields;
        }
    }

    private function getTypes()
    {	
    	$types = array();

        $rsTypes = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('IBLOCK_ID' => $this->types_iblock_id, 'ACTIVE' => 'Y'), false, false, array());

        while($arTypes = $rsTypes->GetNextElement()) {
            $fields = $arTypes->getFields();
            $fields['PROPERTIES'] = $arTypes->getProperties();

            $types[$fields['IBLOCK_SECTION_ID']][] = $fields;
        }

        if (!empty($types)) {
		   	$rsSect = CIBlockSection::GetList(array('SORT' => 'ASC'), array('IBLOCK_ID' => $this->types_iblock_id, 'ACTIVE' => 'Y', 'ID' => array_keys($types)));
		   	while ($arSect = $rsSect->GetNext())
		   	{
		    	$this->arResult['TYPES'][$arSect['ID']] = array(
		    		'NAME' => $arSect['NAME'],
		    		'CHILD' => $types[$arSect['ID']]
		    	);
		   	}
        }
    }

    private function getDoctors()
    {
        $rsDoctors = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('IBLOCK_ID' => $this->doctors_iblock_id, 'ACTIVE' => 'Y'), false, false, array());

        while($arDoctors = $rsDoctors->GetNextElement()) {
            $fields = $arDoctors->getFields();
            $fields['PROPERTIES'] = $arDoctors->getProperties();

            $this->arResult['DOCTORS'][] = $fields;
        }
    }

    private function getItem($id)
    {
        $rsItem = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('IBLOCK_ID' => $this->settings_iblock_id, 'ACTIVE' => 'Y', 'ID' => $id), false, false, array());

        while($arItem = $rsItem->GetNextElement()) {
            $fields = $arItem->getFields();
            $fields['PROPERTIES'] = $arItem->getProperties();
            $fields['POSITIONS'] = $this->getPosition($fields['PROPERTIES']['POSITIONS']['VALUE']);

            $this->arResult['ITEM'] = $fields;
        }
    }

    private function getLogItem($id)
    {
        $rsItem = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('IBLOCK_ID' => $this->log_iblock_id, 'ACTIVE' => 'Y', 'ID' => $id), false, false, array());

        while($arItem = $rsItem->GetNextElement()) {
            $fields = $arItem->getFields();
            $fields['PROPERTIES'] = $arItem->getProperties();

            $this->arResult['LOG_ITEM'] = $fields;
        }
    }

    private function getPosition($fileId)
    {
        $filePath = CFile::GetPath($fileId);

        if (!$filePath) {
            ShowError('JSON не найден');
            die();
        }

        $json = file_get_contents($_SERVER['DOCUMENT_ROOT'] . $filePath);
        $json = json_decode($json, true);

        return $json;
    }

    private function getView()
    {
        $this->pdf(false);

        if (is_file($_SERVER['DOCUMENT_ROOT'] . '/upload/temp.pdf')) {
            echo json_encode(array('success' => true, 'pdf' => '/upload/temp.pdf', 'message' => 'Успешно'));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Ошибка генерации'), JSON_UNESCAPED_UNICODE);
        }

        die();
    }

    private function getSave()
    {
        $this->pdf(true);
        $this->pdf(true, true, true);

        $pdfId = $this->log('Сгенерировать');

        if ($pdfId > 0) {
            echo json_encode(array('success' => true, 'pdfId' => $pdfId, 'message' => 'Сохранено'));
        } else {
            echo json_encode(array('success' => false, 'message' => 'Ошибка генерации'), JSON_UNESCAPED_UNICODE);
        }

        die();
    }

    private function pdf($use_number, $noBG = false, $noIncriment = false)
    {
    	$preview = false;

    	if ($use_number) {
            if (!$noIncriment) {
        		$number = COption::GetOptionString('certificates', 'number');
    			$count = strlen($number);
    			$numb = $number + 1;
    			$new_number = substr($number, 0, $count - strlen($numb)) . $numb;

    			COption::SetOptionString('certificates', 'number', $new_number);
            } else {
                $new_number = COption::GetOptionString('certificates', 'number');
            }
    	} else {
    		$preview = true;
    		$new_number = '000000';
    	}

        require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/juzt/certificates/tcpdf/tcpdf.php');

        $file = $_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($this->arResult['ITEM']['PROPERTIES']['PDF']['VALUE']);
        $outputImagePath = $noBG ? $_SERVER['DOCUMENT_ROOT']. '/upload/tempNoBG.jpg' : $_SERVER['DOCUMENT_ROOT']. '/upload/temp.jpg';
        $font = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/components/juzt/certificates/' . "arial.ttf"; // Ссылка на шрифт

        $font_size = $this->arResult['ITEM']['PROPERTIES']['FONT_SIZE']['VALUE']; // Размер шрифта
        $degree = 0; // Угол поворота текста в градусах

        $size =  getimagesize($file);
        $width = $size[0];
        $height = $size[1];

        if ($noBG) {
        	$pic = imagecreate($size[0], $size[1]);
        	$white = imagecolorallocate($pic, 255, 255, 255);
        	imagefill($pic, 0, 0, $white);
        	ImageColorTransparent($im, $white);
        } elseif (stripos($file, '.png') !== FALSE) {
            $pic = imagecreatefrompng($file);
        } else {
            $pic = imagecreatefromjpeg($file);
        }

        $color = imagecolorallocate($pic, 0, 0, 0);

        $CENTER = $width / 2;

        if ($preview) {
        	$this->watermark($pic, $width, $height, $degree, $font);
        }
        
        if (!empty($this->arResult['ITEM']['POSITIONS'])) {
	        foreach($this->arResult['ITEM']['POSITIONS'] as $fieldName => $fieldValue) {
	            $coords = explode(':', $fieldValue['coords']);

	            $text_a = explode(' ', $_POST[$fieldName]);
				$text_new = [];
				$text_stroke = '';

				if (isset($fieldValue['width'])) {
					if (count($text_a) > 1) {
						foreach($text_a as $word) {
						    $box = imagettfbbox($font_size, $degree, $font, $text_stroke.' '.$word);

						    if ($box[2] > $fieldValue['width']) {
						        $text_new[] = $text_stroke;
						        $text_stroke = $word;
						    } else {
						        $text_stroke .= " ".$word;
						    }
						}

						$text_new[] = $text_stroke;

						$text_new = array_reverse($text_new);
					} else {
						$text_new[] = $_POST[$fieldName];
					}
				} else {
					$text_new[] = $_POST[$fieldName];
				}

				foreach($text_new as $text_key => $text_n) {
		            $box = imagettfbbox($font_size, $degree, $font, $text_n);

		            if (isset($fieldValue['center']) && $fieldValue['center']) {
		                if ($coords[0] == 'center') {
		                    $left = $CENTER-(($box[2]-$box[0]) / 2);
		                } else {
		                    $left = $coords[0]-(($box[2]-$box[0]) / 2);
		                }
		            } else {
		                $left = $coords[0];
		            }

		            if (count($text_new) > 1) {
		            	$top = $coords[1] - ($text_key * ($box[3] - $box[5] + 20));
		            } else {
		            	$top = $coords[1];
		            }

		            imagettftext($pic, $font_size, $degree, $left, $top, $color, $font, $text_n);
		        }
	        }
	    }

        if ($this->arResult['ITEM']['PROPERTIES']['FIO_PERSON_COORDS']['VALUE']) {
        	$str = htmlspecialchars(str_replace(' ','',$_POST['certificate_surname']) . ' ' . str_replace(' ','',$_POST['certificate_name']));

        	$this->fio = $str;

            $box = imagettfbbox($font_size, $degree, $font, $str);

            $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['FIO_PERSON_COORDS']['VALUE']);

            if ($coords[0] == 'center') {
                $left = $CENTER-(($box[2]-$box[0]) / 2);
            } else {
                $left = $coords[0]-(($box[2]-$box[0]) / 2);
            }

            imagettftext($pic, $font_size, $degree, $left, $coords[1], $color, $font, $str);
        }

        if ($this->arResult['ITEM']['PROPERTIES']['TYPE_COORDS']['VALUE']) {
        	$str = htmlspecialchars($_POST['certificate_type']);

            $box = imagettfbbox($font_size, $degree, $font, $str);

            $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['TYPE_COORDS']['VALUE']);

            if ($coords[0] == 'center') {
                $left = $CENTER-(($box[2]-$box[0]) / 2);
            } else {
                $left = $coords[0]-(($box[2]-$box[0]) / 2);
            }

            imagettftext($pic, $font_size, $degree, $left, $coords[1], $color, $font, $str);
        }

        if ($this->arResult['ITEM']['PROPERTIES']['CERTIFICATE_ID_COORDS']['VALUE']) {

            $box = imagettfbbox($font_size, $degree, $font, $new_number);

            $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['CERTIFICATE_ID_COORDS']['VALUE']);

            if ($coords[0] == 'center') {
                $left = $CENTER-(($box[2]-$box[0]) / 2);
            } else {
                $left = $coords[0]-(($box[2]-$box[0]) / 2);
            }

            imagettftext($pic, $font_size, $degree, $left, $coords[1], $color, $font, $new_number);
        }

        if ($this->arResult['ITEM']['PROPERTIES']['DATE_COORDS']['VALUE']) {
            if ($this->arResult['ITEM']['PROPERTIES']['DATE']['VALUE']) {
                $date = date('d.m.Y', strtotime($this->arResult['ITEM']['PROPERTIES']['DATE']['VALUE']));
            } else {
                $date = date('d.m.Y');
            }

            $box = imagettfbbox($font_size, $degree, $font, $date);

            $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['DATE_COORDS']['VALUE']);
            $left = $coords[0]-(($box[2]-$box[0]) / 2);
            imagettftext($pic, $font_size, $degree, $left, $coords[1], $color, $font, $date);
        }

        if ($this->arResult['ITEM']['PROPERTIES']['FIO_COORDS']['VALUE']) {
        	$doctor = htmlspecialchars($_POST['certificate_doctor']);

        	$box = imagettfbbox($font_size, $degree, $font, $doctor);

            $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['FIO_COORDS']['VALUE']);
            $left = $coords[0]-(($box[2]-$box[0]) / 2);
            imagettftext($pic, $font_size, $degree, $left, $coords[1], $color, $font, $doctor);
        }

        if (!$noBG) {
            if ($this->arResult['ITEM']['PROPERTIES']['SIGNATURE']['VALUE'] && $this->arResult['ITEM']['PROPERTIES']['SIGNATURE_COORDS']['VALUE']) {
                $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['SIGNATURE_COORDS']['VALUE']);

                $signature = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($this->arResult['ITEM']['PROPERTIES']['SIGNATURE']['VALUE']));

                $size =  getimagesize($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($this->arResult['ITEM']['PROPERTIES']['SIGNATURE']['VALUE']));
                $widthSign = $size[0];
                $heightSign = $size[1];

                imagecopyresampled($pic, $signature, $coords[0] - $widthSign / 2, $coords[1] - $heightSign / 2, 0, 0, $widthSign, $heightSign, $widthSign, $heightSign);
            } 
        }       

        imagejpeg($pic, $outputImagePath);
        imagedestroy($pic);

        if ($noBG) {
            $pic = imagecreatefromjpeg($outputImagePath);

            if ($this->arResult['ITEM']['PROPERTIES']['SIGNATURE']['VALUE'] && $this->arResult['ITEM']['PROPERTIES']['SIGNATURE_COORDS']['VALUE']) {
                $coords = explode(':', $this->arResult['ITEM']['PROPERTIES']['SIGNATURE_COORDS']['VALUE']);

                $signature = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($this->arResult['ITEM']['PROPERTIES']['SIGNATURE']['VALUE']));

                $size =  getimagesize($_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($this->arResult['ITEM']['PROPERTIES']['SIGNATURE']['VALUE']));
                $widthSign = $size[0];
                $heightSign = $size[1];

                imagecopyresampled($pic, $signature, $coords[0] - $widthSign / 2, $coords[1] - $heightSign / 2, 0, 0, $widthSign, $heightSign, $widthSign, $heightSign);

                imagejpeg($pic, $outputImagePath);
            }

            imagedestroy($pic);
        }

        ob_clean();

        if (!file_exists($outputImagePath)) {
            echo json_encode(array('success' => false, 'message' => 'Не удалось нанести текст на картинку'), JSON_UNESCAPED_UNICODE);
            die();
        }

        $pdf = new TCPDF('P', 'px');

        $pdf->AddPage('P', 'A4');
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetMargins(0, 0, 0);
        $pdf->SetHeaderMargin(0);
        $pdf->SetFooterMargin(0);
        $pdf->SetAutoPageBreak(TRUE, 0);

        $pdf->Image($outputImagePath, 0, 0, $width, $height, '', '', '', false, 300, '', false, false, 0);

        $pdf->Output($noBG ? $_SERVER['DOCUMENT_ROOT']. '/upload/tempNoBG.pdf' : $_SERVER['DOCUMENT_ROOT']. '/upload/temp.pdf', "F");

        if ($noBG) {
            unlink($outputImagePath);
        }
    }

    private function log($type, $url = false, $white = false, $fio = false)
    {
        global $USER;

        $fio =  $fio ? $fio : $this->fio;

        $el = new CIBlockElement;

        if (!$url) {
	        $file = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']. '/upload/temp.pdf');
	        $fileNoBG = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']. '/upload/tempNoBG.pdf');
	        $fileNoBG['name'] = $file["name"] = Cutil::translit($this->arResult['ITEM']['NAME'] . '_' . date('d.m.Y H.i.s'), "ru", array("replace_space"=>"_","replace_other"=>"_")) . '.pdf';

	        $image = new SimpleImage();
		   	$image->load($_SERVER['DOCUMENT_ROOT']. '/upload/temp.jpg');
		   	$image->resizeToWidth(600);
		   	$image->save($_SERVER['DOCUMENT_ROOT']. '/upload/temp.jpg');

		   	$filePreview = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']. '/upload/temp.jpg');
	    } else {
	    	if (intval($white) != 1) {
	    		$file = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']. $url);
	    	} else {
	    		$fileNoBG = CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT']. $url);
	    	}

	    	$filePreview = false;
	    }

        $arLoadProductArray = Array(
            "MODIFIED_BY"    => $USER->GetID(),
            "IBLOCK_SECTION_ID" => false,
            "IBLOCK_ID"      => $this->log_iblock_id,
            "PROPERTY_VALUES"=> array(
                "PDF" => $file,
                "PDF_NO_BG" => $fileNoBG,
                "FIO" => $fio
            ),
            "PREVIEW_PICTURE" => $filePreview,
            "NAME"           => $this->arResult['ITEM']['NAME'] . ' | ' . $type . ' | ' . $fio,
            "ACTIVE"         => "Y",
        );

        if ($id = $el->Add($arLoadProductArray)) {
            unlink($_SERVER['DOCUMENT_ROOT']. '/upload/temp.jpg');
            unlink($file);
            unlink($fileNoBG);

            sleep(1);

            $this->urls[$id] = [
            	'html' => '<a href="' . '/certificates_generator/?id=' . $this->arResult['ITEM']['ID'] . '&logId=' . $id . '" target="_blank">Открыть сертификат</a>  - ' . $fio
            ];

            return $id;
        } else {
            return false;
        }
    }

    private function watermark(&$pic, $width, $height, $degree, $font)
    {
    	$degree = 45;

    	$box = imagettfbbox(65, $degree, $font, 'ПРЕДПРОСМОТР');

    	$color = imagecolorallocatealpha($pic, 0, 0, 0, 100);

    	$boxHeight = $box[3] - $box[5] + 200;
    	$boxHeightCount = ceil($height / $boxHeight);
    	$boxTopMargin = ($height - $boxHeight * $boxHeightCount) / 2 / $boxHeightCount;

    	$boxWidth = $box[2] - $box[0] + 100;
    	$boxWidthCount = floor($width / $boxWidth);
    	$boxLeftMargin = ($width - $boxWidth * $boxWidthCount) / 2;

    	for($i = 0; $i < $boxHeightCount; $i++) {
			for($j = 0; $j < $boxWidthCount; $j++) {
				if ($j == 0) {
					$left = $boxLeftMargin;
				} else {
					$left = $j * $boxWidth + $boxLeftMargin;
				}
				
				if ($i == 0) {
					$top = $boxHeight + $boxTopMargin;
				} else {
					$top = $i * $boxHeight + $boxTopMargin;
				}

    			imagettftext($pic, 65, $degree, $left, $top, $color, $font, 'ПРЕДПРОСМОТР');
    		}
    	}
    }

    private function getMassGenerator()
    {
        require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/juzt/certificates/phpexcel/PHPExcel.php');

        $pExcel = PHPExcel_IOFactory::load($_FILES['file']['tmp_name']);

        $headerInfo = array();
        $importInfo = array();

        foreach ($pExcel->getWorksheetIterator() as $worksheet) {
            foreach($worksheet->toArray() as $array) {
                if (empty($headerInfo)) {
                    $headerInfo = $array;
                } else {
                    $importInfo[] = $array;
                }
            }
        }

        if (empty($importInfo)) {
            echo json_encode(array('success' => false, 'message' => 'Таблица пуста'));
            die();
        }

        $requiredFields = array();

        foreach($headerInfo as $key => $field) {
            if (stripos($field, '*') !== FALSE) {
                $requiredFields[] = $key;
            }
        }

        $customFields = array();
        if (!empty($this->arResult['ITEM']['POSITIONS'])) {
            foreach($this->arResult['ITEM']['POSITIONS'] as $fieldName => $fieldValue) {
                $customFields[] = $fieldName;
            }
        }

        foreach($importInfo as $row => $fields) {
            foreach($requiredFields as $required) {
                if (!$fields[$required]) {
                    echo json_encode(array('success' => false, 'message' => 'Не заполнены обязательные поля. Строка ' . ($row + 2) . ' ячейка ' . ($required + 1)));
                    die();
                }
            }
        }

        foreach($importInfo as $row => $fields) {
            $customKey = -1;

            foreach($fields as $key => $field) {
                switch ($key) {
                    case 0:
                        $_POST['certificate_type'] = $field;
                        break;
                    case 1:
                        $_POST['certificate_doctor'] = $field;
                        break;
                    case 2:
                        $_POST['certificate_surname'] = $field;
                        break;
                    case 3:
                        $_POST['certificate_name'] = $field;
                        break;
                    default:
                        if (count($customFields) > 0) {
                            $customKey++;
                            if ($customKey <= count($customFields) - 1) {
                                $_POST[$customFields[$customKey]] = $field;
                            } else {

                            }
                        }
                        break;
                }
            }

            $this->pdf(true);
            $this->pdf(true, true, true);
            $pdfId = $this->log('Сгенерировать из excel');
        }

        if (!empty($this->urls)) {
            $zip = $this->createZip();
        } else {
            echo json_encode(array('success' => false, 'message' => 'Произошел сбой. Сертификаты не сгенерировались'));
            die();
        }

        echo json_encode(array('success' => true, 'message' => 'Успешно', 'url' => $zip, 'ids' => $this->urls));
    }

    private function createZip()
    {
        $zipPath = $_SERVER['DOCUMENT_ROOT'] . '/upload/' . Cutil::translit($this->arResult['ITEM']['NAME'], "ru", array("replace_space"=>"_","replace_other"=>"_")) . '.zip';

        if (is_file($zipPath)) unlink($zipPath);

        $zip = new ZipArchive();

        if ($zip->open($zipPath, ZIPARCHIVE::CREATE) === TRUE) {

            $rsFiles = CIBlockElement::GetList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('IBLOCK_ID' => $this->log_iblock_id, 'ACTIVE' => 'Y', 'ID' => array_keys($this->urls)), false, false, array());

            $key = 0;

            while($arFiles = $rsFiles->GetNextElement()) {
                $props = $arFiles->getProperties();

                $file1 = CFile::GetFileArray($props['PDF']['VALUE']);
                $file2 = CFile::GetFileArray($props['PDF_NO_BG']['VALUE']);

                $zip->addFile(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($props['PDF']['VALUE'])), $file1['FILE_NAME'] . '.pdf');
                $zip->addFile(str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . CFile::GetPath($props['PDF_NO_BG']['VALUE'])), $file2['FILE_NAME'] . '_NO_BG.pdf');

                $key++;
            }
        } else {
            $zip->close();
            echo json_encode(array('success' => false, 'message' => 'Не удалось создать архив'));
            die();
        }

        $zip->close();

        return '/upload/' . Cutil::translit($this->arResult['ITEM']['NAME'], "ru", array("replace_space"=>"_","replace_other"=>"_")) . '.zip';
    }

    private function getDownloadSimple()
    {
        require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/components/juzt/certificates/phpexcel/PHPExcel.php');
        $fileName = Cutil::translit($this->arResult['ITEM']['NAME'], "ru", array("replace_space"=>"_","replace_other"=>"_")) . '.xlsx';

        $pExcel = new PHPExcel();
        $pExcel->setActiveSheetIndex(0);
        $aSheet = $pExcel->getActiveSheet();

        $aSheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $aSheet->getPageSetup()->SetPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $aSheet->setTitle('Образец');

        $aSheet->setCellValue('A1','Вид/подвид сертификата*');
        $aSheet->setCellValue('B1','Врач*');
        $aSheet->setCellValue('C1','Фамилия*');
        $aSheet->setCellValue('D1','Имя*');

        $word = 'D';

        if (!empty($this->arResult['ITEM']['POSITIONS'])) {
            foreach($this->arResult['ITEM']['POSITIONS'] as $fieldName => $fieldValue) {
                $word++;
                $aSheet->setCellValue($word . '1', $fieldValue['name'] . '*');
            }
        }

        $word++;
        $aSheet->setCellValue($word . '1','Компания');
        $word++;
        $aSheet->setCellValue($word . '1','Клиника');
        $word++;
        $aSheet->setCellValue($word . '1','Пол');
        $word++;
        $aSheet->setCellValue($word . '1','Телефон');
        $word++;
        $aSheet->setCellValue($word . '1','E-mail');

        $objWriter = PHPExcel_IOFactory::createWriter($pExcel, 'Excel2007');
        $objWriter->save($_SERVER['DOCUMENT_ROOT'] . '/upload/' . $fileName);

        echo json_encode(array('success' => true, 'url' => '/upload/' . $fileName));
    }
}



class SimpleImage 
{
	var $image;
	var $image_type;

	function load($filename) 
	{
		$image_info = getimagesize($filename);
		$this->image_type = $image_info[2];
		if( $this->image_type == IMAGETYPE_JPEG ) {
	 		$this->image = imagecreatefromjpeg($filename);
		} elseif( $this->image_type == IMAGETYPE_GIF ) {
	 		$this->image = imagecreatefromgif($filename);
		} elseif( $this->image_type == IMAGETYPE_PNG ) {
	 		$this->image = imagecreatefrompng($filename);
		}
	}

	function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) 
	{
		if( $image_type == IMAGETYPE_JPEG ) {
	 		imagejpeg($this->image,$filename,$compression);
		} elseif( $image_type == IMAGETYPE_GIF ) {
	 		imagegif($this->image,$filename);
		} elseif( $image_type == IMAGETYPE_PNG ) {
	 		imagepng($this->image,$filename);
		}
		if( $permissions != null) {
	 		chmod($filename,$permissions);
		}
	}

	function output($image_type=IMAGETYPE_JPEG) 
	{
		if( $image_type == IMAGETYPE_JPEG ) {
 			imagejpeg($this->image);
		} elseif( $image_type == IMAGETYPE_GIF ) {
	 		imagegif($this->image);
		} elseif( $image_type == IMAGETYPE_PNG ) {
	 		imagepng($this->image);
		}
	}

	function getWidth() 
	{
  		return imagesx($this->image);
	}

	function getHeight() 
	{
  		return imagesy($this->image);
	}

	function resizeToHeight($height)
	{
		$ratio = $height / $this->getHeight();
		$width = $this->getWidth() * $ratio;
		$this->resize($width,$height);
	}

	function resizeToWidth($width)
	{
		$ratio = $width / $this->getWidth();
		$height = $this->getheight() * $ratio;
		$this->resize($width,$height);
	}

	function scale($scale)
	{
		$width = $this->getWidth() * $scale/100;
		$height = $this->getheight() * $scale/100;
		$this->resize($width,$height);
	}

	function resize($width,$height)
	{
		$new_image = imagecreatetruecolor($width, $height);
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
		$this->image = $new_image;
	}
}

?>